#!/bin/bash

# Based on https://gist.github.com/2206527 and version here https://gist.github.com/oodavid/2206527

# Be pretty
echo -e "======================================"
echo -e "S3 MySQL Backup Script v1.0 - drjnet  "
echo -e "======================================"
echo -e ""
echo -e "Local Usage : s3mysqlbackup.sh {mysqlrootpassword} {s3bucketname}"
echo -e "Remote Usage : curl -s https://bitbucket.org/logicc/tools/raw/master/s3mysqlbackup/s3mysqlbackup.sh | bash -s {mysqlrootpassword} {s3bucketname}"
echo -e ""
echo -e ""
echo -e "NOTE: Requires s3cmd package and then run s3cmd --configure with the logiccmysqlbackups bucket IAM user access id and secret key"
echo -e "==============================="

# Basic variables
mysqlpass=$1
bucket="s3://$2"


# Timestamp (sortable AND readable)
daystamp=`date +%Y%m%d`
timestamp=`date +%H%M`

# List all the databases but exclude system ones
databases=`mysql -u root -p$mysqlpass -e "SHOW DATABASES;" | tr -d "| " | grep -v "\(Database\|information_schema\|performance_schema\|mysql\|test\)"`

# Feedback
echo -e "Dumping to \e[1;32m$bucket/$folderstamp/\e[00m"

# Loop the databases
for db in $databases; do

  # Define our filenames
  servername=$(hostname) 
  filename="$db-$daystamp-$timestamp.sql.gz"
  tmpfile="/tmp/$filename"
  object="$bucket/$daystamp/$servername/$filename"

  # Feedback
  echo -e "\e[1;34m$db\e[00m"

  # Dump and zip
  echo -e "  creating \e[0;35m$tmpfile\e[00m"
  mysqldump -u root -p$mysqlpass --force --opt --databases "$db" | gzip -c > "$tmpfile"

  # Upload
  echo -e "  uploading..."
  s3cmd put "$tmpfile" "$object"

  # Delete
  rm -f "$tmpfile"

done;

# Jobs a goodun
echo -e "Backups completed"