#!/bin/bash

# ==============================================================================
# description                   : Backs up all mysql dbs with some exclusions
# author                        : Dave Jenkins
# usage                         : run with no params as root
# ==============================================================================

# Be pretty
echo -e "======================================"
echo -e "MySQL Local Backup (All Databases)"
echo -e "======================================"
echo -e ""
echo -e "Usage : Create a scripts in /root/bin or wherever you prefer with the command below in it (with your desired parameters values) then cron it to run daily, simple...."
echo -e "curl -s https://bitbucket.org/logicc/tools/raw/master/mysqlbackup-local.sh | bash -s {backupPath} {backupRetainDays} {emailTo}"
echo -e "==============================="


# Setup Variables from script parameters
backupPath=$1
backupRetainDays=$2
emailTo=$3

echo $1 $2 $3

# Read server specific settings from ini file
basedir=$(dirname "$0")
echo "Srcript Running in : $basedir"

# Create backup dir if doesnt exist
mkdir -p $backupPath

# [Settings] - see note above
emailTxt=""
serverName=$(hostname)
today=$(date +%Y%m%d)
backupPath+=$today
echo "Backup path : $backupPath"

# grab current list of dbs from mysql and save into array
arr=(`mysql -Bse 'show databases;' | egrep -v 'information_schema|mysql|performance_schema|test'`)

# make dated dir if doesnt exist
mkdir -p $backupPath

# Loop through array, dump and zip each db
echo "Dumping dbs into $backupPath..."
for (( i = 0 ; i < ${#arr[@]} ; i++ ))
do
   mydb=$(echo ${arr[i]} | awk '{print $1}')
   emailtxt+=$mydb
   backupfilename=$backupPath/$mydb.sql.gz
   echo "$mydb ----> $mydb.sql.gz"
   mysqldump $mydb | gzip > $backupfilename
done
echo "....all done."

echo -- Clearing backups older than x days 
find $backupPath/.. -mtime +$backupRetainDays -delete
echo --- Emailing Log ---
echo $emailTxt
echo $emailTxt | mail -s "MySql Backup Complete - "$serverName $emailTo
echo --- Backup Complete, exiting... ---
